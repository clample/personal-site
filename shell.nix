{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.zola
    pkgs.optipng
    pkgs.imagemagick
  ];
}
