+++
template = "index.html"
title = "Chris Lample's Blog"
path = "/"
+++

Hi and welcome to my site! I'm Chris, a software developer living in Brussels.

I wrote a master's thesis - [Evaluating the Impact of Reconfiguration Speed on FPGA Performance](/lample-thesis-evaluating-imact-of-reconfig-speed.pdf).

Before that, I worked on mobile application testing.
I also had the pleasure of going to the [Recurse Center](http://recurse.com) in 2017.

I'd love to hear from you! You can reach me via email at [chris@clample.com](mailto:chris@clample.com).
